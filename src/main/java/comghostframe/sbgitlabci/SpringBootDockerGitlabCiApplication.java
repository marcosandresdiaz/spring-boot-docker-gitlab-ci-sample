package comghostframe.sbgitlabci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDockerGitlabCiApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDockerGitlabCiApplication.class, args);
    }

}
