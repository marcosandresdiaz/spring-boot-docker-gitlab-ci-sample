# Spring Boot Docker Gitlab CI Sample

This guide and project sample show how to use Docker running in a node to run a gitlab-runner container that issues docker commands to the host. This guide uses the `host` docker network.

### 1. Create your runner's `config.toml`
- Create a host directory for your runner config, for example `mkdir /etc/gitlab-runner`.

- Run a temporary (`--rm`) interactive (`-it`) container that will register a runner, that is, add a runner configuration entry to the `config.toml` in the previous config directory. Fill in your registration token from your repo's `Settings > CI / CD > Runners` section:

  ```bash 
  docker run --network=host --rm -it -v /etc/gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner register --non-interactive \
  --name gitlab-runner \
  --url https://gitlab.com/ \
  --executor docker \
  --docker-network-mode host  \
  --docker-image docker:latest \
  --run-untagged \
  --registration-token {{token}}
  ```

It should have created a `/etc/gitlab-runner/config.toml` file with your configuration.

### 2. Start your runner:

Run a container with a runner based on your `config.toml`, with access to your host's docker daemon (`docker.sock`) to be able to run your CI/CD `docker` commands on the host.

  ```bash 
  docker run \
    --detach \
    --network=host \
    --volume /etc/gitlab-runner:/etc/gitlab-runner \
    --volume /var/run/docker.sock:/var/run/docker.sock \
    --name gitlab-runner \
    gitlab/gitlab-runner
  ```

You can see its output with `docker logs -f gitlab/gitlab-runner`

### 3. Set up your pipeline:

Go to your repository and add Gitlab CI configuration and a Dockerfile. 
Check out the examples in this repository.

### 4. Run your pipeline!
