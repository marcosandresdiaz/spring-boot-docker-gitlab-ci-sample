FROM maven:3.6.0-jdk-11-slim
COPY . /
RUN mvn clean package --quiet
ENTRYPOINT java -jar /target/app.jar